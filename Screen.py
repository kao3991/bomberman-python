import pygame
from pygame.locals import *

class Screen(object):
  def __init__(self):
    self.offsetLeft = 140
    self.screen = pygame.display.set_mode( (800,600), pygame.FULLSCREEN );

  def drawField(self, field, x,y):
    self.screen.blit(field.image(), (x*Field.WIDTH+self.offsetLeft, y*Field.HEIGHT))

  def drawPlayer(self, player):
    self.screen.blit(player.image(), ((player.position.x * Field.WIDTH)+player.position.offsetX+self.offsetLeft, (player.position.y * Field.HEIGHT)+player.position.offsetY) )
  def drawBomb(self, bomb):
    self.screen.blit(bomb.image(), ((bomb.position.x * Field.WIDTH)+self.offsetLeft, (bomb.position.y * Field.HEIGHT)))
  def redraw(self):
    pygame.display.flip()

from Field import Field
    
