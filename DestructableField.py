from Field import Field
class DestructableField(Field):
  def image(self):
    return Images.fields['destructable']
  def canWalkOnto(self):
    return False
  
  def blocksExplosion(self):
    return True
  def destructable(self):
    return True 
from Images import Images
