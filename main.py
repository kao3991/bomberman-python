from Controller import Controller
controller = Controller()
controller.initialize()
while True:
    controller.show_menu()
    controller.start_game()
    controller.draw()
    controller.loop()
