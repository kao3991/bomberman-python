from random import randint
class Field(object):
  WIDTH = 40
  HEIGHT = 40
  @staticmethod
  def fieldFor(x,y):
    if (x == 0 and y == 0):
      return EmptyField()
    if (x == 0 and y == 1 or x==1 and y == 0):
      return EmptyField()
    if (x == Map.WIDTH-1 and y == Map.HEIGHT-1):
      return EmptyField()
    if (x == Map.WIDTH-1 and y == Map.HEIGHT-2 or x == Map.WIDTH-2 and y == Map.HEIGHT-1):
      return EmptyField()
    if (x % 2 > 0 and y % 2 > 0):
      return SolidField()
    if (randint(0,3)==0):
      return EmptyField()
    else:
      return DestructableField()
  def __init__(self, bonus = None):
    self._bonus = bonus
  
  def isEmpty(self):
    return False

  def getBonus(self):
    return self._bonus

  def removeBonus(self):
    self._bonus = None

from Map import Map    
from EmptyField import EmptyField
from SolidField import SolidField
from DestructableField import DestructableField
