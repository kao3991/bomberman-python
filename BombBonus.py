from Bonus import Bonus

class BombBonus(Bonus):
  def image(self):
    return Images.bonus['bomb']

  def collect(self, player):
    player.bombLimit += 1

from Images import Images
