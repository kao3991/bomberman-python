class Bomb(object):
  def __init__(self, player):
    self.range = player.bombRange
    self.position = player.position.place()
    self.ttl = 2500
    self.player = player
    self.active = 1
  def decrement(self, miliseconds):
    self.ttl -= miliseconds
    
  def image(self):
    return Images.bomb[self.state()]

  def state(self):
    state = int(round(self.ttl/500.0))
    if state < 0:
      state = 0
    if state > 4:
      state = 4
    return state 

  def detonated(self):
    if self.active:
      self.active = False
      self.player.bombCounter -= 1

from Images import Images
    
