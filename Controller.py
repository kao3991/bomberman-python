import sys
import pygame 
from pygame.locals import *
from Map import Map
from time import sleep
class Controller(object):

  def initialize(self):
    pygame.init()
    pygame.joystick.init()
    pygame.font.init()
    self.screen = Screen() 
    self.joystick = pygame.joystick.Joystick(0)
    self.joystick.init()
    pygame.mouse.set_visible(0)
  def show_menu(self):
    self.screen.screen.blit(Images.splashscreen, (0,0))
    pygame.display.flip()
    sleep(3);
    menu = Menu(self)     
    menu.show()
    
  def start_game(self):
    self.isEnded = False
    pygame.mixer.init()
    self.play_song('music/background.ogg', 0.3, True)
    self.map = Map.generate()
    self.map.placePlayers(self.firstPlayer, self.secondPlayer)
    self.winningTimeout = False
    self.winner = None
    pygame.draw.rect(self.screen.screen, (0,0,0), Rect(0,0,800,600))

  def draw(self):
    for x in range(0, Map.WIDTH):
      for y in range(0, Map.HEIGHT):
        self.drawField(x,y)
    for bomb in self.map.bombs:
      self.drawBomb(bomb)
    for explosion in self.map.explosions:
      self.drawBomb(explosion)
    for player in self.map.players:
      self.drawPlayer(player)
    self.screen.redraw()

  def drawField(self, x,y):
    self.screen.drawField(self.map.fieldAt(x,y), x,y)
  def drawPlayer(self, player):
    self.screen.drawPlayer(player)
  def drawBomb(self, bomb):
    self.screen.drawBomb(bomb)
  def loop(self):
    clock = pygame.time.Clock()
    while not self.isEnded:
      self.tick(clock.tick())

  def tick(self, miliseconds):
    self.g_keys = pygame.event.get()
    for event in self.g_keys:
      pass

    self.updateDirections()
    self.movePlayers(miliseconds)
    self.detonateBombs()
    self.updateBombs(miliseconds)
    self.updateGameStatus(miliseconds)
    self.placeBombs()
    self.collectBonuses()
    self.draw()
  def updateBombs(self, miliseconds):
    for bomb in self.map.bombs:
      bomb.decrement(miliseconds)
    for explosion in self.map.explosions:
      if explosion.ttl <= 0:
        self.map.explosions.remove(explosion)
      else:
        explosion.decrement(miliseconds)
      

  def detonateBombs(self):
    for bomb in self.map.bombs:
      if bomb.ttl < 0:
        self.detonateBomb(bomb)
    for bomb in self.map.bombs:
      if bomb.ttl < 0:
        self.map.bombs.remove(bomb)
        
  def addExplosion(self, x,y):
    self.map.explosions.append(Explosion(x,y)) 

  def detonateBomb(self, bomb):
    self.play_song('music/explosion_2.ogg', 1.0)
    for x in range(0,4):
      direction = x+1
      bomb.ttl = -1;
      pos = bomb.position
      self.addExplosion(pos.x, pos.y)
      for player in self.map.players:
        if player.position.x == pos.x and player.position.y == pos.y:
          player.kill()
      for distance in range(0, bomb.range):
        try:
          pos = pos.getInDirection(direction)
          if not self.map.isValidPosition(pos.x, pos.y):
            break
          field = self.map.fieldAt(pos.x, pos.y)
          bomb.detonated()
          for player in self.map.players:
            if player.position.x == pos.x and player.position.y == pos.y:
              player.kill()
          if field.destructable():
            self.addExplosion(pos.x, pos.y)
            self.map.destroyField(pos.x, pos.y)
          if (field.isEmpty()):
            self.addExplosion(pos.x, pos.y)
          detonated = False
          for bmb in self.map.bombs:
            if bmb.ttl > 0:
              if bmb.position.x == pos.x and bmb.position.y == pos.y:
                self.detonateBomb(bmb)
                detonated = True
          if detonated:
            break
          if field.blocksExplosion():
            break

        except IndexError:
          break
  def collectBonuses(self):
    for player in self.map.players:
      field = self.map.fieldAt(player.position.x, player.position.y)
      bonus = field.getBonus()
      if bonus is not None:
        self.play_song('music/achievment.ogg', 1.0)
        bonus.collect(player)
        field.removeBonus()

  def updateGameStatus(self, miliseconds):
    aliveCount = 0
    for player in self.map.players:
      if player.alive:
        aliveCount += 1
    if aliveCount <= 1:
      if self.winningTimeout is False:
        self.winningTimeout = 3000
      else:
        self.winningTimeout -= miliseconds
    if self.winningTimeout is not False:
      if self.winningTimeout <= 0:
        for player in self.map.players:
          if player.alive:
            self.setWinner(player)
        self.endGame()

  def setWinner(self, player):
    self.winner = player

  def endGame(self):
    self.isEnded = True
    self.screen.screen.blit(Images.end_bg, (2,0))
    if self.winner is not None:
      label = Images.menuFont.render("Winner is: "+Player.destriptionByCol(self.winner.color), 0, (255,255,255))
    else:
      label = Images.menuFont.render("Draw game", 0, (255,255,255))
    self.screen.screen.blit(label, (150,250))
    pygame.display.flip()
    sleep(10)
    return
  def movePlayers(self, miliseconds):
    for player in self.map.players:
      fractionOfSecond = miliseconds/1000.0
      if (self.map.isValidMove(player, player.direction)):
        if (player.direction == Direction.LEFT):
          player.position.offsetX -= (player.speed * Field.WIDTH) * fractionOfSecond
        elif (player.direction == Direction.RIGHT):
          player.position.offsetX += (player.speed * Field.WIDTH) * fractionOfSecond
        elif (player.direction == Direction.UP):
          player.position.offsetY -= (player.speed * Field.HEIGHT) * fractionOfSecond
        elif (player.direction == Direction.DOWN):
          player.position.offsetY += (player.speed * Field.HEIGHT) * fractionOfSecond
        player.position.recalculate()

  def placeBombs(self):
    if self.joystick.get_button(4):
      self.placeBomb(self.redPlayer())
    if self.joystick.get_button(5):
      self.placeBomb(self.whitePlayer())
  def placeBomb(self, player):
    if player.alive:
      x = player.position.x
      y = player.position.y
      for bomb in self.map.bombs:
        if bomb.position.x == x and bomb.position.y == y:
          return;
      bmb2 = player.getBomb()
      if bmb2:
        self.map.bombs.append(bmb2);
     

  def whitePlayer(self):
     return self.map.players[1]
  
  def redPlayer(self):
     return self.map.players[0]

  def updateDirections(self):
    whitePlayer = self.whitePlayer()
    whiteMoving = False 
    if self.joystick.get_button(0):
      whitePlayer.direction = Direction.UP
      whiteMoving = True
    elif self.joystick.get_button(2):
      whitePlayer.direction = Direction.DOWN
      whiteMoving = True
    if self.joystick.get_button(3):
      whitePlayer.direction = Direction.LEFT
      whiteMoving = True
    elif self.joystick.get_button(1):
      whitePlayer.direction = Direction.RIGHT
      whiteMoving = True
    if not whiteMoving:
      whitePlayer.direction = Direction.NONE

    redPlayer = self.redPlayer()
    redMoving = False 
    if self.joystick.get_button(6):
      redPlayer.direction = Direction.UP
      redMoving = True
    elif self.joystick.get_axis(1) > 0.2:
      redPlayer.direction = Direction.DOWN
      redMoving = True
    if self.joystick.get_axis(0) < -0.2:
      redPlayer.direction = Direction.LEFT
      redMoving = True
    elif self.joystick.get_button(7):
      redPlayer.direction = Direction.RIGHT
      redMoving = True
    if not redMoving:
      redPlayer.direction = Direction.NONE
  def play_song(self, song_name, volume, repeat = False):
    snd1 = pygame.mixer.Sound(song_name)
    snd1.set_volume(volume)    
    if repeat is True:
      snd1.play(-1)
    else:
      snd1.play()
from Player import Player
from Bomb import Bomb
from Field import Field
from Screen import Screen
from Images import Images
from Direction import Direction
from Explosion import Explosion
from Menu import Menu
