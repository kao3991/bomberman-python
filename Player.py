class Player(object):
  def __init__(self, position, color):
    self.direction =0 
    self.position = position
    self.color = color
    self.speed = 2 
    self._image = Images.players[self.color][4]
    self.bombLimit = 1
    self.bombRange = 2 
    self.bombCounter = 0
    self.alive = True

  def image(self):
    if not self.alive:
      return Images.players['dead']
    if (self.direction > 0):
      self._image = Images.players[self.color][self.direction]
    return self._image

  def getBomb(self):
    if self.bombCounter < self.bombLimit:
      self.bombCounter += 1
      return Bomb(self)
  def kill(self):
    self.alive = False
  @staticmethod
  def players():
    return [ 'ani', 'paw', 'luk', 'mac'  ]
  @staticmethod
  def destriptionByCol(col):
    if col == 'ani':
      return "Flow Chancellor"
    if col == 'paw':
      return "The Force of eEngine"
    if col == 'luk':
      return "Master Architect"
    if col == 'mac':
      return "Human2Human Ambassador"
      
from Bomb import Bomb
from Images import Images


