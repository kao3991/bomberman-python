from Field import Field
class SolidField(Field):
  def image(self):
    return Images.fields['solid']
  def canWalkOnto(self):
    return False
  
  def onDestroy(self):
    pass

  def blocksExplosion(self):
    return True
  
  def destructable(self):
    return False 
from Images import Images
