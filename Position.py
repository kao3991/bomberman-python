class Position(object):
  def __init__(self, x,y):
    self.x = x
    self.y = y
    self.offsetX = 0
    self.offsetY = 0
  def getLeft(self):
    return Position(self.x - 1, self.y)
  def getRight(self):
    return Position(self.x + 1, self.y)
  def getUp(self):
    return Position(self.x, self.y -1)
  def getDown(self):
    return Position(self.x, self.y + 1)
  def getInDirection(self, d):
    if (d == Direction.LEFT):
      return self.getLeft()
    if (d == Direction.RIGHT):
      return self.getRight()
    if (d == Direction.UP):
      return self.getUp()
    if (d == Direction.DOWN):
      return self.getDown()

  def recalculate(self):
    if (self.offsetX > Field.WIDTH/2):
      self.x += 1
      self.offsetX = (self.offsetX * -0.99)
    elif (self.offsetX < -Field.WIDTH/2):
      self.x -= 1
      self.offsetX = (self.offsetX * -0.99 )
    if (self.offsetY > Field.HEIGHT/2):
      self.y += 1
      self.offsetY = (self.offsetY * -0.99)
    elif (self.offsetY < -Field.WIDTH/2):
      self.y -= 1
      self.offsetY = (self.offsetY * -0.99)

  def place(self):
    return Position(self.x, self.y)
    
from Field import Field
from Direction import Direction

