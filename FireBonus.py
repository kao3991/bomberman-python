from Bonus import Bonus

class FireBonus(Bonus):
  def image(self):
    return Images.bonus['fire']

  def collect(self, player):
    player.bombRange += 1

from Images import Images
