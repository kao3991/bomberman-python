import pygame
from pygame.locals import * 
pygame.init()
pygame.font.init()

class Images:
  fields = {
    'destructable': pygame.image.load('assets/field-destructable.png'),
    'solid': pygame.image.load('assets/field-solid.png'),
    'empty': pygame.image.load('assets/field-empty.png')
  }
  splashscreen = pygame.image.load('assets/SPLASHSCREEN.png')
  menu_bg = pygame.image.load('assets/menu_bg.png')
  end_bg = pygame.image.load('assets/winner_bg.png')
  players = {
    'dead' : pygame.image.load('assets/GHOST.png'), 
    'ani' : {
      1: pygame.image.load('assets/ANI_1.png'),  
      2: pygame.image.load('assets/ANI_2.png'),  
      3: pygame.image.load('assets/ANI_3.png'),  
      4: pygame.image.load('assets/ANI_4.png')  
    },
    'paw' : {
      1: pygame.image.load('assets/PAW_1.png'),  
      2: pygame.image.load('assets/PAW_2.png'),  
      3: pygame.image.load('assets/PAW_3.png'),  
      4: pygame.image.load('assets/PAW_4.png')  
    },
    'mac' : {
      1: pygame.image.load('assets/MAC_1.png'),  
      2: pygame.image.load('assets/MAC_2.png'),  
      3: pygame.image.load('assets/MAC_3.png'),  
      4: pygame.image.load('assets/MAC_4.png')  
    },
    'luk' : {
      1: pygame.image.load('assets/LUK_1.png'),  
      2: pygame.image.load('assets/LUK_2.png'),  
      3: pygame.image.load('assets/LUK_3.png'),  
      4: pygame.image.load('assets/LUK_4.png')  
    }
  }
  bomb = {
    0: pygame.image.load('assets/BOMBA_0.png'),
    1: pygame.image.load('assets/BOMBA_1.png'),
    2: pygame.image.load('assets/BOMBA_2.png'),
    3: pygame.image.load('assets/BOMBA_3.png'),
    4: pygame.image.load('assets/BOMBA_4.png')
  }
  explosion = {
    0: pygame.image.load('assets/WYBUCH_1.png')
  }
  bonus = {
    'fire': pygame.image.load('assets/BONUS_FIRE.png'), 
    'bomb': pygame.image.load('assets/BONUS_BOMB.png'), 
    'speed': pygame.image.load('assets/BONUS_SPEED.png') 
  }
  menuFont = pygame.font.Font("assets/ARCADECLASSIC.TTF", 36)

