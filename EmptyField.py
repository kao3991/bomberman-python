from Field import Field
class EmptyField(Field):
  def image(self):
    if self._bonus is not None:
      return self._bonus.image()
    else:
      return Images.fields['empty']

  def canWalkOnto(self):
    return True

  def blocksExplosion(self):
    return False 
  def destructable(self):
    if self._bonus is not None:
      return True
    else:
      return False 

  def isEmpty(self):
    return True
from Images import Images
from Bonus import Bonus
