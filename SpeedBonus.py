from Bonus import Bonus

class SpeedBonus(Bonus):
  def image(self):
    return Images.bonus['speed']

  def collect(self, player):
    player.speed += 0.2

from Images import Images
