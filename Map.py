
class Map(object): 
  WIDTH = 13
  HEIGHT = 13
  @staticmethod
  def generate():
    maap = Map()
    for x in range(0, Map.WIDTH):
      if (x <= len(maap.fields)):
        maap.fields.append([])
      for y in range(0, Map.HEIGHT):
        maap.fields[x].append( Field.fieldFor(x,y))
    return maap

  def __init__(self):
    self.fields = [] 
    self.players = []
    self.bombs = []
    self.explosions = []
  
  def placePlayers(self, firstPlayer, secondPlayer):
    self.players.append(Player(Position(0,0), firstPlayer))
    self.players.append(Player(Position(self.WIDTH-1,self.HEIGHT-1), secondPlayer))
  def randomBonus(self):
    number = randint(0,10)
    if number == 7:
      return FireBonus()
    if number == 8:
      return BombBonus()
    if number == 9:
      return SpeedBonus()
    return None

  def fieldAt(self,x,y):
    return self.fields[x][y]
  def destroyField(self, x, y):
    if self.fieldAt(x,y) is EmptyField:
      self.fields[x][y] = EmptyField()
    else:
      self.fields[x][y] = EmptyField(self.randomBonus()) 

  def isValidMove(self, player, direction):
    valid = True
    movingOutOfField = False
    if (direction == Direction.LEFT):
      if (player.position.offsetX <= 0):
        movingOutOfField = True
        if (player.position.x == 0):
          valid = False;
    if (direction == Direction.RIGHT):
      if (player.position.offsetX >= 0):
        movingOutOfField = True
        if (player.position.x == Map.WIDTH-1):
          valid = False;
    if (direction == Direction.UP):
      if (player.position.offsetY <= 0):
        movingOutOfField = True
        if (player.position.y == 0):
          valid = False;
    if (direction == Direction.DOWN):
      if (player.position.offsetY >= 0):
        movingOutOfField = True
        if (player.position.y == Map.HEIGHT-1):
          valid = False;
    if not movingOutOfField:
      return True
    if (valid):
      destination = player.position.getInDirection(direction)
      if not self.fieldAt(destination.x, destination.y).canWalkOnto():
        valid = False
      else:
        for bomb in self.bombs:
          if bomb.position.x == destination.x and bomb.position.y == destination.y:
            valid = False
    return valid

  def isValidPosition(self, x,y):
    if x < 0 or x > Map.WIDTH -1:
      return False
    if y < 0 or y > Map.HEIGHT -1:
      return False
    return True

from Field import Field
from Player import Player
from Position import Position
from Direction import Direction
from EmptyField import EmptyField
from random import randint
from FireBonus import FireBonus
from BombBonus import BombBonus
from SpeedBonus import SpeedBonus

