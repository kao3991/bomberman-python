class Menu(object):
  def __init__(self, controller):
    self.ctrl = controller
    self.firstPlayer = None
    self.secondPlayer = None
    self.player1Index = 0
    self.player2Index = 1
    pygame.init()
    pygame.font.init()
    self.lastState = {6:False, 1: False, 0:False, 2:False}
    self.joystick = pygame.joystick.Joystick(0)
    self.joystick.init()
  def show(self):
    self.playersSelector()
     
  def playersSelector(self):
    top = 100
    left = 200
    players =  Player.players()
    while True:
      g_keys = pygame.event.get()
      for event in g_keys:
        pass
      self.ctrl.screen.screen.blit(Images.menu_bg, (2,0))
      for row in range(0, len(players)):
        rowTop = (60*row)+top
        p = players[row]
        self.ctrl.screen.screen.blit(Images.players[p][4], (left,rowTop))
        label = Images.menuFont.render(Player.destriptionByCol(p), 0, (255,255,255))
        self.ctrl.screen.screen.blit(label, (left+40, rowTop))
      p1color = (255,255,255) if self.firstPlayer is None else (255,0,0)
      p2color = (255,255,255) if self.secondPlayer is None else (255,0,0)
      p1label = Images.menuFont.render("P1", 0, p1color)
      p2label = Images.menuFont.render("P2", 0, p2color)
      
      self.ctrl.screen.screen.blit(p1label, (left-60, top+60*self.player1Index))
      self.ctrl.screen.screen.blit(p2label, (left+500, top+60*self.player2Index))
      if self.firstPlayer is None:
        if self.joystick.get_button(6):
          if self.lastState[6] is False:
            self.player1Index -= 1
            while self.player1Index == self.secondPlayer or self.player1Index < 0:
              if self.player1Index == self.secondPlayer:
                self.player1Index -= 1
              if self.player1Index < 0:
                self.player1Index = len(players)-1
          self.lastState[6] = True
        else:
          self.lastState[6] = False
        if self.joystick.get_axis(1) > 0.2:
          if self.lastState[1] is False:
            self.player1Index += 1
            while self.player1Index == self.secondPlayer or self.player1Index >= len(players):
              if self.player1Index >= len(players):
                self.player1Index = 0
              if self.player1Index == self.secondPlayer:
                self.player1Index += 1
          self.lastState[1] = True
        else:
          self.lastState[1] = False
        if self.joystick.get_button(4):
          if self.player1Index != self.secondPlayer:
            self.firstPlayer = self.player1Index

      if self.secondPlayer is None:
        if self.joystick.get_button(0):
          if self.lastState[0] is False:
            self.player2Index -= 1
            while self.player2Index == self.firstPlayer or self.player2Index < 0:
              if self.player2Index == self.firstPlayer:
                self.player2Index -= 1
              if self.player2Index < 0:
                self.player2Index = len(players)-1
          self.lastState[0] = True
        else:
          self.lastState[0] = False

        if self.joystick.get_button(2):
          if self.lastState[2] is False:
            self.player2Index += 1
            while self.player2Index == self.firstPlayer or self.player2Index >= len(players):
              if self.player2Index == self.firstPlayer:
                self.player2Index += 1
              if self.player2Index >= len(players):
                self.player2Index = 0
          self.lastState[2] = True
        else:
          self.lastState[2] = False

        if self.joystick.get_button(5):
          if self.player2Index != self.firstPlayer:
            self.secondPlayer = self.player2Index
       
      pygame.display.flip()
      if self.firstPlayer is not None and self.secondPlayer is not None and not self.joystick.get_button(5) and not self.joystick.get_button(4):
        self.ctrl.firstPlayer = players[self.firstPlayer]
        self.ctrl.secondPlayer = players[self.secondPlayer]
        return
        

      
 

      
      





import pygame 
from pygame.locals import *
from Images import Images
from Player import Player
