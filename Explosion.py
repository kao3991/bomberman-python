class Explosion(object):
  def __init__(self, x, y):
    self.ttl = 1000
    self.position = Position(x,y)

  def decrement(self, milis):
    self.ttl -= milis

  def image(self):
    return Images.explosion[0]
   
from Images import Images
from Position import Position
